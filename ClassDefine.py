import matplotlib as mpl
mpl.use('TkAgg')
from matplotlib import pyplot as plt
import matplotlib.cm as cm
import math
from PIL import Image

class Player():
    def __init__(self, maze, mazeSetup):
        self.hp = 100
        self.attack = 10
        self.defence = 10
        self.maze = maze
        self.mazeSetup = mazeSetup
        self.x = 0
        self.y = 0
        self.imageHandler = ''
    def currentCell(self):
        return self.maze[self.y, self.x]

class GameStatus():
    def __init__(self, maze, player, setup):
        self.maze = maze
        self.player = player
        self.setup = setup

    def initialize(self):
        fig = plt.figure(2)
        player = self.player
        plt.get_current_fig_manager().window.wm_geometry('200x480+680+10')
        self.fig = fig
        # basic monster atk 15, def 0, hp 50
        basic_cost = (math.floor(50/player.attack) + 1) * (15-player.defence)
        # hard monster atk 18, def 5, hp 100
        hard_cost = (math.floor(100 / (player.attack - 5)) + 1) * (18 - player.defence)

        self.img_basic = Image.open('./basic.png')
        self.img_basic.thumbnail((14, 14), Image.ANTIALIAS)

        self.img_hard = Image.open('./hard.png')
        self.img_hard.thumbnail((14, 14), Image.ANTIALIAS)

        self.text_player = fig.text(0.05, 0.95, 'Player ', transform=fig.transFigure)
        self.text_hp = fig.text(0.05, 0.92, 'HP: ' + str(player.hp), transform=fig.transFigure)
        self.text_atk = fig.text(0.05, 0.89, 'Attack: ' + str(player.attack), transform=fig.transFigure)
        self.text_def = fig.text(0.05, 0.86, 'Defence: ' + str(player.defence), transform=fig.transFigure)
        self.text_basic= fig.text(0.05, 0.80, 'Moster: ', transform=fig.transFigure)
        self.text_basic_cost = fig.text(0.55, 0.80, 'Cost: ' + str(basic_cost), transform=fig.transFigure)
        self.text_hard= fig.text(0.05, 0.77, 'Moster: ', transform=fig.transFigure)
        self.text_hard_cost = fig.text(0.55, 0.77, 'Cost: ' + str(hard_cost), transform=fig.transFigure)
        fig.figimage(self.img_hard, 200*0.4, 0.77*480 - 28, origin='upper', zorder=10)
        fig.figimage(self.img_basic, 200 * 0.4, 0.77 * 480 - 14, origin='upper', zorder=10)

    def update(self):
        player = self.player
        basic_cost = max((math.floor(50 / player.attack) + 1) * (15 - player.defence), 0)
        # hard monster atk 18, def 5, hp 100
        hard_cost = max((math.floor(100 / (player.attack - 5)) + 1) * (18 - player.defence), 0)

        self.text_hp.set_text('HP: ' + str(player.hp))
        self.text_atk.set_text('Attack: ' + str(player.attack))
        self.text_def.set_text('Defence: ' + str(player.defence))
        self.text_basic_cost.set_text('Cost: ' + str(basic_cost))
        self.text_hard_cost.set_text('Cost: ' + str(hard_cost))



