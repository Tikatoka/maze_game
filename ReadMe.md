<h1 id='title'>Mini Game - Maze </h1>

**Author: Ruiheng Li**  
**Version: 0.1**

**Description:**  
A maze is generate via recursive backtracker method. `mazeGenerator.py` provides a dynamical demonstration 
of the maze generation process. Set `dynamics = True` to see this. Run `mazeGame.py` to start playing.