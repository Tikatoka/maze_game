import numpy as np
import random
import matplotlib as mpl
mpl.use('TkAgg')
from matplotlib import pyplot as plt
import matplotlib.cm as cm
from PIL import Image
import math


def generateMazeBackTraker (numCols, numRows, dynamics = False):
    # the Maze stored as 3D matrix the 5 elements in last dimession is the wall in W, E, N, S and visited check
    maze = np.zeros((numRows, numCols, 5))
    # the current cell coordinates
    current_x = 0
    current_y = 0
    # push the start point into visited history list
    visitedHistory = [(current_x,current_y)]
    # for dynamic demonstration only
    visited = []
    while visitedHistory:
        # mark the current cell as visited
        maze[current_x, current_y, 4] = 1
        if (dynamics):
            plotMaze(maze, dynamics, visited, (current_x, current_y))
        # store the ever visited cell for demonstration
        visited.append((current_x, current_y))
        # start check the cells around the current cell and push the unvisited in to choice list
        # initial the checklist as an empty array
        choiceList = []
        if current_x > 0 and maze[current_x - 1, current_y, 4] == 0:
            choiceList.append('N')
        if current_x < (numRows-1) and maze[current_x + 1, current_y, 4] == 0:
            choiceList.append('S')
        if current_y > 0 and maze[current_x, current_y - 1 , 4] == 0:
            choiceList.append('W')
        if current_y < (numCols - 1) and maze[current_x, current_y + 1, 4] == 0:
            choiceList.append('E')
        # choose one direction and propagate
        if (len(choiceList)):
            move = random.choice (choiceList)
            visitedHistory.append((current_x, current_y))
            # 0-3 W E N S
            # 0-3 L U R D
            # 0 2 1 3
            if move == 'W':
                maze[current_x, current_y, 0] = 1
                current_y -= 1
                maze[current_x, current_y, 1] = 1
            if move == 'E':
                maze[current_x, current_y, 1] =1
                current_y += 1
                maze[current_x, current_y, 0] = 1
            if move == 'N':
                maze[current_x, current_y, 2] = 1
                current_x -= 1
                maze[current_x, current_y, 3] = 1
            if move == 'S':
                maze[current_x, current_y, 3] = 1
                current_x += 1
                maze[current_x, current_y, 2] = 1
        else:
            previousCell = visitedHistory.pop()
            current_x = previousCell[0]
            current_y = previousCell[1]
    # open the start and end port
    maze[0,0,0] = 0
    maze[numRows-1, numCols-1, 1] = 1
    return maze

def plotMaze(maze, dynamics = False, visited = False, current = False):
    # prepare the image data
    global player
    shapeMaze = np.shape(maze)
    numRows = shapeMaze[0]
    numCols = shapeMaze[1]
    imageData = np.zeros((numRows*10, numCols*10))
    # start plotting
    for i in range (0,numRows):
        for j in range (0,numCols):
            oneCell = maze[i,j]
            for m in range (10*i+2, 10*i+8):
                imageData[m,range(10*j+2, 10*j+8)] = 255
            if oneCell[0] == 1:
                imageData[range(10*i+2,10*i+8),10*j] = 255
                imageData[range(10*i+2,10*i+8),10*j+1] = 255
            if oneCell[2] == 1:
                imageData[10 * i, range(10 * j + 2, 10 * j + 8)] = 255
                imageData[10 * i + 1, range(10 * j + 2, 10 * j + 8)] = 255
            if oneCell[1] == 1:
                imageData[range(10 * i + 2, 10 * i + 8), 10 * j + 9] = 255
                imageData[range(10 * i + 2, 10 * i + 8), 10 * j + 8] = 255
            if oneCell[3] == 1:
                imageData[10*i+9,range(10*j+2,10*j+8)] = 255
                imageData[10*i+8,range(10*j+2,10*j+8)] = 255
    if (dynamics):
        # paint the visited cell into grey if the dynamics is on
        for cell in visited:
            rowNum = cell[0]
            colNum = cell[1]
            for r in range (10*rowNum + 2, 10*rowNum + 8):
                imageData[r, range(10 * colNum + 2, 10 * colNum + 8)] = 180
        for r in range (10*current[0] +2, 10*current[0] + 8):
            imageData[r, range(10 * current[1] + 2, 10 * current[1] + 8)] = 20
    figure = plt.gcf()

    fig = plt.imshow(imageData, cmap=cm.Greys_r, interpolation='none')

    # remove the axes and box from figure
    # figure = plt.figure(1)
    # figure.canvas.mpl_connect('key_press_event', pressd)

    fig.axes.get_xaxis().set_visible(False)
    fig.axes.get_yaxis().set_visible(False)
    plt.box(on=None)

    if (dynamics):
        plt.show(block=False)
        plt.pause(0.05)
        plt.clf()
    else:
        # plt.show()
        print(1)


def displayInteractiveObjects(setup, maze, player):
    fig = plt.gcf()
    # load the images and resize them
    img_player = Image.open('./player.png')
    img_player.thumbnail((9, 9), Image.ANTIALIAS)

    img_potion =  Image.open('./heart.png')
    img_potion.thumbnail((9, 9), Image.ANTIALIAS)

    img_basic = Image.open('./basic.png')
    img_basic.thumbnail((9, 9), Image.ANTIALIAS)

    img_hard = Image.open('./hard.png')
    img_hard.thumbnail((9, 9), Image.ANTIALIAS)

    img_atk = Image.open('./atk.png')
    img_atk.thumbnail((9, 9), Image.ANTIALIAS)

    img_defence = Image.open('./defence.png')
    img_defence.thumbnail((9, 9), Image.ANTIALIAS)

    # draw the player
    ox, oy = cellPositionToPixelPosition([player.x, player.y],maze)
    player.imageHandler = fig.figimage(img_player, ox, oy, origin='upper', zorder=10)

    for key in setup:
        if isinstance(key, tuple) and setup[key]['type'] == 'hp_potion':
            ox, oy = cellPositionToPixelPosition([setup[key]['position'][0], setup[key]['position'][1]], maze)
            setup[key]['imageHandler'] = fig.figimage(img_potion, ox, oy, origin='upper', zorder=10)

        if isinstance(key, tuple) and setup[key]['type'] == 'monster_basic':
            ox, oy = cellPositionToPixelPosition([setup[key]['position'][0], setup[key]['position'][1]], maze)
            setup[key]['imageHandler'] = fig.figimage(img_basic, ox, oy, origin='upper', zorder=10)

        if isinstance(key, tuple) and setup[key]['type'] == 'monster_hard':
            ox, oy = cellPositionToPixelPosition([setup[key]['position'][0], setup[key]['position'][1]], maze)
            setup[key]['imageHandler'] = fig.figimage(img_hard, ox, oy, origin='upper', zorder=10)

        if isinstance(key, tuple) and setup[key]['type'] == 'atk_gem':
            ox, oy = cellPositionToPixelPosition([setup[key]['position'][0], setup[key]['position'][1]], maze)
            setup[key]['imageHandler'] = fig.figimage(img_atk, ox, oy, origin='upper', zorder=10)

        if isinstance(key, tuple) and setup[key]['type'] == 'def_gem':
            ox, oy = cellPositionToPixelPosition([setup[key]['position'][0], setup[key]['position'][1]], maze)
            setup[key]['imageHandler'] = fig.figimage(img_defence, ox, oy, origin='upper', zorder=10)



def cellPositionToPixelPosition(cellPosition, maze):
    # xRange is a 2 elements list [xminpixcels, xmaxpixels]
    # yRange is a 2 elements list [yminpixcels, ymaxpixels]
    # cellPosition is [x, y]
    # mazeDimenssion is [maxRows, maxCols]
    shapeMaze = np.shape(maze)
    numRows = shapeMaze[0]
    numCols = shapeMaze[1]
    fig = plt.figure(1)
    ax = plt.gca()
    pixelMin = ax.transData.transform([0, 0])
    pixelMax = ax.transData.transform([numCols * 10, numRows * 10])
    xRange = [pixelMin[0], pixelMax[0]]
    yRange = [pixelMin[1], pixelMax[1]]
    x, y = cellPosition
    maxRows, maxCols = numRows, numCols
    xAxisPosition = x * 10 + 2
    yAxisPosition = y * 10 + 6
    xOffsetInPixel = int((xRange[1] - xRange[0]) * xAxisPosition / (maxCols * 10) + xRange[0])
    yOffsetInPixel = int((yRange[1] - yRange[0]) * yAxisPosition / (maxRows * 10) + yRange[0])

    return [xOffsetInPixel, yOffsetInPixel]


def generageGameObjectsSetup(maze):
    # find the size of the maze
    shapeMaze = np.shape(maze)
    numRows = shapeMaze[0]
    numCols = shapeMaze[1]
    setup = {'rows': numRows, 'cols': numCols}

    hp_potion = int(numRows*numCols/50)
    monster_basic = int(numRows*numCols/50)
    monster_hard = int(numRows * numCols / 10)
    atk_gem = int(numRows * numCols / 100)
    def_gem = int(numRows * numCols / 100)

    fillOneTypeObject(setup, 'hp_potion', hp_potion, numRows, numCols)
    fillOneTypeObject(setup, 'monster_basic', monster_basic, numRows, numCols)
    fillOneTypeObject(setup, 'monster_hard', monster_hard, numRows, numCols)
    fillOneTypeObject(setup, 'atk_gem', atk_gem, numRows, numCols)
    fillOneTypeObject(setup, 'def_gem', def_gem, numRows, numCols)

    return setup

def fillOneTypeObject(setup, key, number, mazeX, mazeY):
    keymap = ['hp_potion', 'monster_basic', 'monster_hard', 'atk_gem', 'def_gem']


    while(number):
        cell_x = random.randint(1, mazeX - 1)
        cell_y = random.randint(1, mazeY - 1)
        if (cell_x, cell_y) not in setup:
            number -= 1
            setup[(cell_x, cell_y)] = {'type': key, 'position': (cell_x, cell_y)}

def refreshMaze(setup, maze, player):
    ox, oy = cellPositionToPixelPosition([player.x, player.y], player.maze)
    player.imageHandler.ox = ox
    player.imageHandler.oy = oy


    for key in setup:
        if isinstance(key, tuple):
            ox, oy = cellPositionToPixelPosition([setup[key]['position'][0], setup[key]['position'][1]], maze)
            setup[key]['imageHandler'].ox = ox
            setup[key]['imageHandler'].oy = oy

def enterNewCell(maze, player, setup):
    cell = (player.x, player.y)
    if cell not in setup:
        return
    if setup[cell]['imageHandler']._alpha == 0:
        return
    if setup[cell]['type'] == 'hp_potion':
        player.hp += 100
        setup[cell]['imageHandler'].set_alpha(0)
        return

    if setup[cell]['type'] == 'atk_gem':
        player.attack += 2
        setup[cell]['imageHandler'].set_alpha(0)
        return

    if setup[cell]['type'] == 'def_gem':
        player.defence += 1
        setup[cell]['imageHandler'].set_alpha(0)
        return

    if setup[cell]['type'] == 'monster_basic':
        basic_cost = max((math.floor(50/player.attack) + 1) * (15-player.defence),0)
        player.hp -= basic_cost
        setup[cell]['imageHandler'].set_alpha(0)
        return

    if setup[cell]['type'] == 'monster_hard':
        hard_cost = max((math.floor(100 / (player.attack - 5)) + 1) * (18 - player.defence),0)
        player.hp -= hard_cost
        setup[cell]['imageHandler'].set_alpha(0)
        return




if (__name__ == '__main__'):
    maze = generateMazeBackTraker(20,20, dynamics=False)
    plotMaze(maze, dynamics=False)