import mazeGenerator as mG
import ClassDefine as pC
import time

def keyPress(event):
    global game_over, game_start
    if game_over:
        return
    if game_start:
        game_start = True
    else:
        if event.key == 'right' and player.currentCell()[1] == 1:
            player.x += 1
        if event.key == 'left' and player.currentCell()[0] == 1:
            player.x -= 1
        if event.key == 'up' and player.currentCell()[2] == 1:
            player.y -= 1
        if event.key == 'down' and player.currentCell()[3] == 1:
            player.y += 1
    status.update()
    mG.enterNewCell(maze, player, mazeSetup)
    mG.refreshMaze(mazeSetup, maze, player)
    mG.plt.figure(1).canvas.draw()
    if player.hp <= 0:
        game_over = True
        mG.plt.gcf().text(0.5, 0.5, 'GameOver ', transform=mG.plt.gcf().transFigure, color='r', fontsize='x-large')


global game_over, game_start
game_start = False
game_over = False
# prepare the maze
maze = mG.generateMazeBackTraker(numCols=20, numRows=20)
mazeSetup = mG.generageGameObjectsSetup(maze)

# prepare the player
player = pC.Player(maze, [])


# prepare the plot
mG.plotMaze(maze)
mG.displayInteractiveObjects(mazeSetup, maze, player)

# bind the key press function
mG.plt.figure(1).canvas.mpl_connect('key_press_event', keyPress)
status = pC.GameStatus(maze, player, mazeSetup)
status.initialize()
# render the maze
mG.plt.show()


